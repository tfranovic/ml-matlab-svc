function [x,y,alphas, b, k, s] = trainSVM(x, y, C, type, s)
%TRAINSVM Training SVM for classification
if(strcmp(type,'linear'))
    k=@k_lin;
    s=0;
else
    k=@k_gau;
end
%get K matrix
K=getK(x,k,s);

%get alphas
alphas=getAlphas(K,y,C);

%get R
b=getB(x,y,alphas,C,k,s);
end