function [ b ] = getB(x, y, alphas, C, k, s)
%GETB Calculation borderline distance
%   Calculation if the distance of the hyperplane from the origin
b=0;
N=size(x,1);
n=0;
for i=1:N
    if(alphas(i)>0 && alphas(i)<C)
        sums=0;
        for j=1:N
            sums=sums+alphas(j)*y(j)*k(x(j,:),x(i,:),s);
        end
        b=b+y(i)-sums;
        n=n+1;
    end
end
b=b/n;
if (isnan(b))
    b=0;
end

