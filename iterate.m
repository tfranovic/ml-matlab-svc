trainData=load('usps_3_vs_8_train_X.txt');
trainLabels=load('usps_3_vs_8_train_y.txt');
testData=load('usps_3_vs_8_test_X.txt');
testLabels=load('usps_3_vs_8_test_y.txt');

N=size(trainData,1);
%normalize data
mu = mean(trainData);
sigma = std(trainData);
trainData = (trainData - repmat(mu, N, 1)) ./ repmat(sigma, N, 1);
testData = (testData - repmat(mu, N, 1)) ./ repmat(sigma, N, 1);

%iterate C={0.0001,0.001,0.01,0.1,1}
Cs=[0.0001,0.001,0.01,0.1,1];
errV=zeros(size(Cs,1),1);
errT=zeros(size(Cs,1),1);
nsv=zeros(size(Cs,1),1);
t=1;
for C = Cs
    [x,y,alphas,b,k,s] = trainSVM(trainData,trainLabels,C,'linear');
    y2=decide(trainData,x,y,alphas,b,k,s);
    errV(t)=(size(y2(y2~=trainLabels),1)/size(trainLabels,1))*100;
    y2=decide(testData,x,y,alphas,b,k,s);
    errT(t)=(size(y2(y2~=testLabels),1)/size(testLabels,1))*100;
    nsv(t)=(size(alphas(alphas>0),1)/size(x,1))*100;
    t=t+1;
end
figure();
hold on;
plot(log10(Cs),errV,'-Ob');
plot(log10(Cs),errT,'-Or');
xlabel('log10(C)');
ylabel('Misclassification error');
axis([-4 0 0 10])
figure();
plot(log10(Cs),nsv,'-Ob');
xlabel('log10(C)');
ylabel('Support vectors');
axis([-4 0 0 100])

ss=[8,16,32,64,128];
errV=zeros(size(ss,1),1);
errT=zeros(size(ss,1),1);
nsv=zeros(size(ss,1),1);
C=1;
t=1;
for s = ss
   [x,y,alphas,b,k] = trainSVM(trainData,trainLabels,C,'gaussian',s);
    y2=decide(trainData,x,y,alphas,b,k,s);
    errV(t)=(size(y2(y2~=trainLabels),1)/size(trainLabels,1))*100;
    y2=decide(testData,x,y,alphas,b,k,s);
    errT(t)=(size(y2(y2~=testLabels),1)/size(testLabels,1))*100;
    nsv(t)=(size(alphas(alphas>0),1)/size(x,1))*100;
    t=t+1;
end
figure();
hold on;
plot(log2(ss),errV,'-Ob');
plot(log2(ss),errT,'-Or');
xlabel('log2(s)');
ylabel('Misclassification error');
axis([3 7 0 10])
figure();
plot(log2(ss),nsv,'-Ob');
xlabel('log2(s)');
ylabel('Support vectors');
axis([3 7 0 100])