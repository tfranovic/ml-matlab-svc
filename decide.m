function [ f ] = decide(xx, x, y, alphas, b, k, s)
%DECIDE Decision function for SVC
%   Decision function for a support vector machine for classification
N=size(xx,1);
M=size(x,1);
f=zeros(N,1);
for i=1:N
    f(i)=0;
    for j=1:M
        f(i)=f(i)+alphas(j)*y(j)*k(x(j,:),xx(i,:),s);
    end
    f(i)=sign(f(i)+b);
end
end
