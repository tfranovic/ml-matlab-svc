function [ alphas ] = getAlphas(K, y, C)
%GETALPHAS Calculation of support vector coefficients
%   Function that computes the support vector coefficients from the kernel matrix
N=size(K,1);
H = (y*y').*K;
f = -1*ones(N,1);
A = [];
b = [];
Aeq = y';
beq = 0;
lb = zeros(N, 1);
ub = C * ones(N, 1);
options = optimset('Algorithm', 'interior-point-convex','Display','off');
alphas=quadprog(H,f,A,b,Aeq,beq,lb,ub,[],options);
alphas(alphas < C * 0.001) = 0;
alphas(alphas > C * 0.999) = C;
end